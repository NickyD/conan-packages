from conans import ConanFile, CMake, tools


class OpenjpegConan(ConanFile):
    name = "openjpeg"
    version = "1.4"


    homepage = "https://github.com/uclouvain/openjpeg"
    license = "BSD 2-Clause"
    description =  "OpenJPEG is an open-source JPEG 2000 codec written in C language."
    topics =  ('conan', 'jpeg2000', 'jp2', 'openjpeg', 'image', 'multimedia', 'format', 'graphics')
    generators = 'cmake'
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def source(self):
        self.run("git clone https://vcs.firestormviewer.org/3p-libraries/3p-openjpeg" )

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="3p-openjpeg/openjpeg_v1_4_sources_r697")
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

