from conans import ConanFile, CMake, tools
import os

class Nghttp2Conan(ConanFile):
    name = "nghttp2"
    version = "1.42.0"
    license = "MIT"
    url = "https://github.com/nghttp2/nghttp2"
    description = "nghttp2 is an implementation of HTTP/2 and its header compression algorithm HPACK in C."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True, "libxml2:iconv" : False }
    generators = "cmake"
    exports = "*.diff"

    deps =  [
        "zlib/1.2.11",
        "libcurl/7.73.0",
        "openssl/1.1.1h",
        "libxml2/2.9.10" ,
         ""
    ]
    
    def requirements(self):
        for d in self.deps:
            if len(d) > 0:
                self.requires(d)
    
    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        tools.get( "https://github.com/nghttp2/nghttp2/releases/download/v1.42.0/nghttp2-1.42.0.tar.bz2" )
        tools.patch( patch_file=os.path.join(self.recipe_folder, "nghttp-142.diff" ) )

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="nghttp2-1.42.0")
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

