from conans import ConanFile, CMake, tools


class NdphysicsstubConan(ConanFile):
    name = "ndphysicsstub"
    version = "1.0"
    options = {"shared": [True, False], "fPIC": [True, False ] }
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"

    def source(self):
        self.run("git clone https://vcs.firestormviewer.org/3p-libraries/3p-ndPhysicsStub")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="3p-ndPhysicsStub")
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = [
            "nd_Pathing",
            "nd_hacdConvexDecomposition",
            "hacd"
        ]
                               
                              

