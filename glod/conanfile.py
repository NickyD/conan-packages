from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os


class GlodConan(ConanFile):
    name = "glod"
    version = "1.0.p3"
    license = "GLOD Open Source"
    author = "Jonathan Cohen, Nat Duca, Chris Niski, Johns Hopkins University and David Luebke, Brenden Schubert, University of Virginia."
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True,False], "fPIC": [True, False]}
    default_options = { "shared": False, "fPIC": True } 
    generators = "cmake"
    exports = "*.diff"

    @property
    def _source_folder(self):
        return "3p-glod"
    def source(self):
        self.run("git clone https://bitbucket.org/lindenlab/3p-glod")

        if self.options.shared == False:
            os.chdir( "3p-glod")
            tools.patch( patch_file=os.path.join(self.recipe_folder, "static-build.diff" ) )

    def build(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        autotools =AutoToolsBuildEnvironment(self)
        autotools.make()

    def package(self):
        self.copy("glod.h", dst="include/glod/", src="3p-glod/include/")
        if self.options.shared == False:
            self.copy("*.a", dst="lib/", src="3p-glod/lib/")
            self.copy("*.a", dst="lib/", src="3p-glod/src/", keep_path=False)
        else:
            self.copy("libGLOD.so", dst="lib/", src="3p-glod/lib/")
            
    def package_info(self):
        if self.options.shared == False:
            self.cpp_info.libs = ["libGLOD.a", "libvds.a"]
        else:
            self.cpp_info.libs = tools.collect_libs(self)

