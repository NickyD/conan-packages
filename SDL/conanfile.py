from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os

class SDLConan(ConanFile):
    name = "SDL"
    version = "1.2.15"
    license = "LGPL"
    url = "<Package recipe repository url here, for issues about the package>"
    description = """Simple DirectMedia Layer is a cross-platform development library designed to provide low level access to audio, keyboard, mouse, joystick, and graphics hardware via OpenGL and Direct3D."""

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    exports = "*.diff"

    @property
    def _source_folder(self):
        return "SDL-1.2.15"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def source(self):
        tools.get("https://www.libsdl.org/release/SDL-1.2.15.tar.gz")
        os.chdir( self._source_folder )
        tools.patch( patch_file=os.path.join(self.recipe_folder, "SDL-1.2.15.diff" ) )
        if self.settings.os == "Linux" and self.settings.arch == "armv8":
            from shutil import copy
            copy( "/usr/share/automake-1.16/config.guess", "build-scripts/config.guess" )


    def build(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        self.run( "./autogen.sh" )
        autotools =AutoToolsBuildEnvironment(self)
        if self.options.shared == False:
            autotools.configure(args=["--disable-shared"])
        else:
            autotools.configure(args=["--disable-static"])
        autotools.make()

    def package(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        autotools =AutoToolsBuildEnvironment(self)
        autotools.install()
        autotools.make(args=["clean"])


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)


