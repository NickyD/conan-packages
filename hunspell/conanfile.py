from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os


class HunspellConan(ConanFile):
    name = "hunspell"
    version = "1.7.0"

    description = "Hunspell project"
    license = "MPL 1.1, LGPL 2.1, GPL 2"
    url = "https://github.com/hunspell/hunspell.git"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True,False], "fPIC": [True, False]}
    default_options = { "shared": False, "fPIC": True } 

    @property
    def _source_folder(self):
        return "hunspell"

    def build_requirements( self ):
        self.build_requires("libtool/2.4.6")
    
    def source(self):
        git = tools.Git(folder="hunspell")
        git.clone( "https://github.com/hunspell/hunspell", "v1.7.0" )

    def build(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        self.run( "autoreconf -vfi" )
        autotools =AutoToolsBuildEnvironment(self)
        if self.options.shared == False:
            autotools.configure(args=["--disable-shared"])
        else:
            autotools.configure(args=["--disable-static"])
        autotools.make()

    def package(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        autotools =AutoToolsBuildEnvironment(self)
        autotools.install()
        autotools.make(args=["clean"])


    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

