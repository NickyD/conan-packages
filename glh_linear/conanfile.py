from conans import ConanFile, tools


class GlLinearConan(ConanFile):
    name = "gl_linear"
    version = "1.0"
    description = "glh - is a platform-indepenedent C++ OpenGL helper library"
    exports_sources = "include/*"
    no_copy_source = True

    def package(self):
        self.copy("*.h")
