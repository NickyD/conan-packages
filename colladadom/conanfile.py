
from conans import ConanFile, CMake, tools
import os

class ColladadomConan(ConanFile):
    name = "colladadom"
    version = "2.3"
    license = "MIT"
    author = "Sony Computer Entertainment Inc."
    url = "http://sourceforge.net/projects/collada-dom"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    options = {"shared": [True,False], "fPIC": [True, False]}
    default_options = { "pcre:build_pcrecpp": True,
                        "pcre:with_bzip2": False,
                        "pcre:shared": False,
                        "boost:bzip2": False,
                        "boost:layout": "system",
                        "boost:shared": False,
                        "shared": False,
                        "fPIC": True}
    build_requires = "pkg-config_installer/0.29.2@bincrafters/stable"
    exports = "*.diff"
    
    @property
    def _source_folder(self):
        return "collada-dom-2.3.1"
    
    def requirements(self):
        self.requires( "boost/1.74.0")
        self.requires( "libxml2/2.9.10" )
        self.requires( "zlib/1.2.11" )
        self.requires( "minizip/1.2.11" )
        self.requires( "pcre/8.44" )
        pass
        
    def source(self):
        tools.get( "https://sourceforge.net/projects/collada-dom/files/Collada%20DOM/Collada%20DOM%202.3/collada_dom-2.3.1-src.tgz" )
        tools.patch( patch_file=os.path.join(self.recipe_folder, "collada-231.diff" ) )

        if self.options.shared == False:
            tools.replace_in_file( "collada-dom-2.3.1/dom/src/1.4/CMakeLists.txt",
                                   "add_library(collada14dom SHARED ${COLLADA_BASE_SOURCES} ${dom_files})",
                                   "add_library(collada14dom STATIC ${COLLADA_BASE_SOURCES} ${dom_files})" )

    
    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_CXX_FLAGS"] = "-Wno-deprecated-declarations"
        cmake.definitions["OPT_COLLADA15"] = False
        cmake.configure(source_folder=self._source_folder)
        cmake.build()


    def package(self):
        #self.copy("*.h", dst="include", src=self.lib_src)

        cmake = CMake(self)
        cmake.install()
        pass

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        pass
