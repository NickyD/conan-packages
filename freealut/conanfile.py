from conans import ConanFile, CMake, tools
import os

class FreealutConan(ConanFile):
    name = "freealut"
    version = "1.1.0"
    license = "LGPL"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True }
    generators = "cmake"
    requires = "openal/1.21.0"
    exports_sources = "freealut-1.1.0/*"
    
    def source(self):
        if self.options.shared == False:
            tools.replace_in_file( "freealut-1.1.0/CMakeLists.txt",
                                   "ADD_LIBRARY(alut SHARED ${ALUT_SOURCES})",
                                   "ADD_LIBRARY(alut STATIC ${ALUT_SOURCES})" )

        pass

    def build(self):
        print(dir(self))
        cmake = CMake(self)
        cmake.definitions["OPENAL_INCLUDE_DIR"] = self.deps_cpp_info["openal"].include_paths[0]
        cmake.parallel = False
        cmake.configure(source_folder="freealut-1.1.0")
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

