from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os


class XmlrpcEpiConan(ConanFile):
    name = "xmlrpc-epi"
    version = "0.54.2"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    options = {"shared": [True,False], "fPIC": [True, False]}
    default_options = { "shared": False, "fPIC": True } 
    requires = ["libiconv/1.16","expat/2.4.1"]

    @property
    def _source_folder(self):
        return "xmlrpc-epi-0.54.2"
    
    def source(self):
        tools.get( "https://sourceforge.net/projects/xmlrpc-epi/files/xmlrpc-epi-base/0.54.2/xmlrpc-epi-0.54.2.tar.bz2/download", filename="xmlrpc-epi-0.54.2.tar.bz2")
        if self.settings.os == "Linux" and self.settings.arch == "armv8":
            os.chdir( self._source_folder ) 
            from shutil import copy
            copy( "/usr/share/automake-1.16/config.guess", "config.guess" )
        
    def build(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        autotools =AutoToolsBuildEnvironment(self)
        if self.options.shared == False:
            autotools.configure(args=["--disable-shared"])
        else:
            autotools.configure(args=["--disable-static"])
        
        autotools.make()

    def package(self):
        os.chdir(os.path.join(self.build_folder, self._source_folder))
        autotools =AutoToolsBuildEnvironment(self)
        autotools.install()
        autotools.make(args=["clean"])

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)

